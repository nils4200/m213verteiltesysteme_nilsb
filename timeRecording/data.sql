INSERT INTO nilsb_projects (projectsName) VALUES
                                              ('Project Alpha'),
                                              ('Project Beta'),
                                              ('Project Gamma'),
                                              ('Project Delta');

INSERT INTO nilsb_employees (employeesFirstname, employeesLastname) VALUES
                                                                        ('John', 'Doe'),
                                                                        ('Jane', 'Smith'),
                                                                        ('Alice', 'Johnson');

INSERT INTO nilsb_recordingTypes (recordingTypesName) VALUES
                                                          ('Development'),
                                                          ('Testing'),
                                                          ('Design'),
                                                          ('Documentation'),
                                                          ('Meeting');

INSERT INTO nilsb_timeRecordings (employeesId, projectsId, recordingTypesId, timeRecordingsDate, timeRecordingsDuration, timeRecordingsComment) VALUES
                                                                                                                                                    (1, 1, 1, '2024-05-01', 4.50, 'Initial development work'),
                                                                                                                                                    (2, 1, 2, '2024-05-02', 3.00, 'Testing initial module'),
                                                                                                                                                    (3, 2, 3, '2024-05-03', 2.75, 'Designing the interface'),
                                                                                                                                                    (1, 2, 4, '2024-05-04', 1.50, 'Writing documentation'),
                                                                                                                                                    (2, 3, 5, '2024-05-05', 2.00, 'Project meeting'),
                                                                                                                                                    (3, 4, 1, '2024-05-06', 3.25, 'Further development work'),
                                                                                                                                                    (1, 3, 2, '2024-05-07', 4.00, 'Testing second module'),
                                                                                                                                                    (2, 4, 3, '2024-05-08', 2.50, 'Designing the backend'),
                                                                                                                                                    (3, 1, 4, '2024-05-09', 3.75, 'Updating documentation'),
                                                                                                                                                    (1, 2, 5, '2024-05-10', 1.25, 'Team meeting for project update');

INSERT INTO nilsb_statistics (statisticsType, statisticsDate, projectId) VALUES
                                                                             (1, '2024-05-01', 1),
                                                                             (2, '2024-05-02', 1),
                                                                             (2, '2024-05-03', 2),
                                                                             (1, '2024-05-04', 2),
                                                                             (3, '2024-05-05', 3);
