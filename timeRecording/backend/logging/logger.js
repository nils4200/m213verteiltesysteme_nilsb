import amqp from 'amqplib';

async function send(eventData){
    const url = 'amqp://localhost'
    const q = "log"
    const conn = await amqp.connect(url);
    const channel = await conn.createChannel();

    await channel.assertExchange(q, 'fanout', { durable: false });

    const msg = JSON.stringify(eventData);
    channel.publish(q, '', Buffer.from(msg));
    console.log(" [x] Sent %s", msg);

    setTimeout(function() {
        conn.close();
    }, 500);
}

function search(projectId){
    knex("nilsb_projects").select("projectsName").where("projectsId", projectId).limit(1).then((data) => {
        send({type: "Suchen", project: data[0].projectsName||"unknown"});
    })
}

function deleted(employeeId,projectId, recordingTypeId, duration, comment){
    knex("nilsb_employees").select("employeesFirstname", "employeesLastname").where("employeesId", employeeId).limit(1).then((data) => {
        const employee = data[0].employeesFirstname + " " + data[0].employeesLastname;
        knex("nilsb_projects").select("projectsName").where("projectsId", projectId).limit(1).then((data) => {
            const project = data[0].projectsName;
            knex("nilsb_recordingTypes").select("recordingTypesName").where("recordingTypesId", recordingTypeId).limit(1).then((data) => {
                const typeName = data[0].recordingTypesName;
                send({type: "Löschen", employee, project, typeName, duration, comment});
            })
        })
    })
}

function added(employeeId,projectId, recordingTypeId, date, duration, comment, id){
    knex("nilsb_employees").select("employeesFirstname", "employeesLastname").where("employeesId", employeeId).limit(1).then((data) => {
        const employee = data[0].employeesFirstname + " " + data[0].employeesLastname;
        knex("nilsb_projects").select("projectsName").where("projectsId", projectId).limit(1).then((data) => {
            const project = data[0].projectsName;
            knex("nilsb_recordingTypes").select("recordingTypesName").where("recordingTypesId", recordingTypeId).limit(1).then((data) => {
                const typeName = data[0].recordingTypesName;
                send({type: "Hinzufügen", id, employee, project, typeName, date, duration, comment});
            })
        })
    })
}

function edited(id, employeeId,projectId, recordingTypeId, date, duration, comment){
    knex("nilsb_employees").select("employeesFirstname", "employeesLastname").where("employeesId", employeeId).limit(1).then((data) => {
        const employee = data[0].employeesFirstname + " " + data[0].employeesLastname;
        knex("nilsb_projects").select("projectsName").where("projectsId", projectId).limit(1).then((data) => {
            const project = data[0].projectsName;
            knex("nilsb_recordingTypes").select("recordingTypesName").where("recordingTypesId", recordingTypeId).limit(1).then((data) => {
                const typeName = data[0].recordingTypesName;
                send({type: "Editieren", id, employee, project, typeName, date, duration, comment});
            })
        })
    })
}

export {search, deleted, added, edited}
