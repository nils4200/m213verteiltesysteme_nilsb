import amqp from 'amqplib';

async function logger() {
    const q = "log"
    const url = 'amqp://localhost'
    const logs = []

    const conn = await amqp.connect(url);
    const channel = await conn.createChannel();

    await channel.assertExchange(q, 'fanout', {durable: false});

    const assertQueue = await channel.assertQueue('', {exclusive: true});
    channel.bindQueue(assertQueue.queue, q, '');

    channel.consume(assertQueue.queue, msg => {
        process.stdout.write('\x1Bc')
        const messageContent = msg.content.toString();
        const currentDate = new Date().toISOString().slice(0, 10);
        logs.unshift(new Date().toISOString() + " " + messageContent);
        if(logs.length>20) logs.pop();
        console.log(logs.join('\n'));

    }, {noAck: true});
}

logger();
