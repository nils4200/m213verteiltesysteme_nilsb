import fs from 'fs';
import amqp from 'amqplib';
import path from 'path';

async function fileLogger() {
    const q = "log"
    const url = 'amqp://localhost'

    const conn = await amqp.connect(url);
    const channel = await conn.createChannel();

    await channel.assertExchange(q, 'fanout', {durable: false});

    const assertQueue = await channel.assertQueue('', {exclusive: true});
    channel.bindQueue(assertQueue.queue, q, '');

    channel.consume(assertQueue.queue, msg => {
        const messageContent = msg.content.toString();
        const currentDate = new Date().toISOString().slice(0, 10);
        const fileName = path.join(".", `${currentDate}_nilsb.log`);
        fs.appendFileSync(fileName, new Date().toISOString() + " " + messageContent + '\n');
    }, {noAck: true});
}

fileLogger();
