import express from "express";
import {router as dataRouter} from './data/data.routes.js';
import {router as miscRouter} from './misc/misc.routes.js';
import {router as statisticsRouter} from './statistics/statistics.routes.js';
import session from "express-session";
import Keycloak from "keycloak-connect";
import cors from "cors";
import knex from "knex";

const memoryStore = new session.MemoryStore();
const keycloak = new Keycloak({store: memoryStore});

 global.knex = knex({
    client: 'mysql2',
    connection: {
        host: 'sql11.freemysqlhosting.net',
        port: 3306,
        user: 'sql11690338',
        password: '2yn4rDZNTG',
        database: 'sql11690338',
    },
});


const app = express();
const port = 3000;
app.use(cors());

app.use(keycloak.middleware());

app.use(session({
    //secret: found in client timeRecordingBackend, Credentials, Client Secret
    secret: 'CQGSZEILsyeHI6XuUoMv2QdpvvR4qyAQ',
    resave: false,
    saveUninitialized: true,
    store: memoryStore
}));


// frontend is a separate component with a different

// for parsing application/json in order to access data from post requests
app.use(express.json());


app.use('/api/data', keycloak.protect(), dataRouter);
app.use('/api/misc',keycloak.protect(), miscRouter);
app.use('/api/statistics', keycloak.protect(), statisticsRouter);

app.get('/', (req, res) => {
    res.send('Welcome to my server!');
});

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
