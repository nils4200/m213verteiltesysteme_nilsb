async function searchTimeRecordingsForProject(projectId) {
    console.log('searchTimeRecordingsForProject:', projectId);
    return await knex("nilsb_timeRecordings").select().where("projectsId", projectId).then((data) => {
        console.log(data);
        return data;
    });
}

async function deleteTimeRecordingModel(timeRecordingId) {
    return await knex("nilsb_timeRecordings").select().where("timeRecordingsid", timeRecordingId).then((data) => {
        console.log(data);
        knex("nilsb_timeRecordings").delete().where("timeRecordingsid", timeRecordingId).then(() => {
            console.log("deleted");
        })
            .catch();
        return data.length>0?data[0]:null;
    });
}

async function addTimeRecordingModel(timeRecording) {
    console.log('addTimeRecordingModel:', timeRecording);
    return await knex("nilsb_timeRecordings").insert({
        employeesId: timeRecording.employeeId,
        projectsId: timeRecording.projectId,
        recordingTypesId: timeRecording.recordingTypeId,
        timeRecordingsDate: timeRecording.date,
        timeRecordingsDuration: timeRecording.duration,
        timeRecordingsComment: timeRecording.comment
    }).then((data) => {
        console.log("inserted", data);
        return data[0];
    });
}

function editTimeRecordingModel(timeRecording) {
    console.log('editTimeRecordingModel:', timeRecording);
    knex("nilsb_timeRecordings").update({
        employeesId: timeRecording.employeeId,
        projectsId: timeRecording.projectId,
        recordingTypesId: timeRecording.recordingTypeId,
        timeRecordingsDate: timeRecording.date,
        timeRecordingsDuration: timeRecording.duration,
        timeRecordingsComment: timeRecording.comment
    }).where("timeRecordingsid", timeRecording.timeRecordingId).then((data) => {
        console.log("updated");
    });
    return timeRecording.timeRecordingId;
}

export {
    addTimeRecordingModel,
    editTimeRecordingModel,
    searchTimeRecordingsForProject,
    deleteTimeRecordingModel
};
