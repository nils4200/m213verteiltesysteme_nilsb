import { addTimeRecordingModel, editTimeRecordingModel, searchTimeRecordingsForProject, deleteTimeRecordingModel } from './data.model.js';
import {search, deleted, added, edited} from "../logging/logger.js";
import {addCreation, addDeletion, addModification} from "../statistics/statistics.model.js";

async function addTimeRecording(request, response) {
    let timeRecording = request.body; // { projectId: projectId, employeeId: employeeId, recordingTypeId: recordingTypeId, date: date, duration: duration, comment: comment };
    console.log('addTimeRecording', timeRecording);
    const timeRecordingId = await addTimeRecordingModel(timeRecording);
    console.log('timeRecording added with id', timeRecordingId);
    added (timeRecording.employeeId, timeRecording.projectId, timeRecording.recordingTypeId, timeRecording.date, timeRecording.duration, timeRecording.comment, timeRecordingId)
    addCreation(timeRecording.projectId)
    response.json(timeRecordingId);
}
async function editTimeRecording(request, response) { //todo
    let timeRecording = request.body;
    console.log('createTimeRecording', timeRecording);
    const timeRecordingId = await editTimeRecordingModel(timeRecording);
    console.log('timeRecording edited with id', timeRecordingId);
    edited(timeRecordingId, timeRecording.employeeId, timeRecording.projectId, timeRecording.recordingTypeId, timeRecording.date, timeRecording.duration, timeRecording.comment)
    addModification(timeRecording.projectId)
    response.json(timeRecordingId);
}

async function deleteTimeRecording(request, response) {
    let id = request.params.id;
    console.log('deleteTimeRecording', id);
    const timeRecording = await deleteTimeRecordingModel(id);
    console.log('timeRecording deleted with id', timeRecording?.timeRecordingsid);
    if(timeRecording !== null) {
    deleted(timeRecording.employeesId, timeRecording.projectsId, timeRecording.recordingTypesId, timeRecording.timeRecordingsDuration, timeRecording.timeRecordingsComment)
    addDeletion(timeRecording.projectsId)
        }
    response.json(timeRecording?.timeRecordingsid);
}

async function searchTimeRecordings(request, response) {
    let projectId = request.params.projectId;
    console.log('searchTimeRecordings', request.params);
    console.log('searchTimeRecordings', projectId);
    const timeRecordings = await searchTimeRecordingsForProject(projectId);
    search(projectId);
    response.json(timeRecordings);
}

export { addTimeRecording, editTimeRecording, searchTimeRecordings, deleteTimeRecording };
