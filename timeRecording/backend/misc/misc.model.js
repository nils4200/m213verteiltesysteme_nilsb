const statisticTypes = [
  { "name": "deleted", "id": 1 },
  { "name": "new", "id": 2 },
  { "name": "modified", "id": 3 }
];

async function readEmployees() {
  return await knex("nilsb_employees").select().then((data) => data);
}

async function readProjects() {
  return await knex("nilsb_projects").select().then((data) => data)
}

async function readRecordingTypes() {
  return await knex("nilsb_recordingTypes").select().then((data) => data)
}

function readStatisticTypes() {
  return statisticTypes;
}

export {
  readEmployees,
  readProjects,
  readRecordingTypes,
  readStatisticTypes
};
