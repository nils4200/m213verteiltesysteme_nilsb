// router.js
import express from "express";
const router = express.Router();

import statisticController from './statistics.controller.js';

router.get('/deleted/:projectId/:startDate/:endDate', statisticController.getDeletedRecords);
router.get('/added/:projectId/:startDate/:endDate', statisticController.getAddedRecords);
router.get('/modified/:projectId/:startDate/:endDate', statisticController.getModifiedRecords);

export { router };
