function getModifiedRecords(req, res) {
    const projectId = req.params.projectId;
    const startDate = req.params.startDate;
    const endDate = req.params.endDate;
    knex("nilsb_statistics").count().where({
        statisticsType: 3,
        projectId: projectId
    })
        .where('statisticsDate', '>=', startDate)
        .where('statisticsDate', '<=', endDate).then((data) => {
        res.json(Object.values(data[0])[0]);
    });
}

function getAddedRecords(req, res) {
    const projectId = req.params.projectId;
    const startDate = req.params.startDate;
    const endDate = req.params.endDate;
    knex("nilsb_statistics").count().where({
        statisticsType: 2,
        projectId: projectId
    })
        .where('statisticsDate', '>=', startDate)
        .where('statisticsDate', '<=', endDate).then((data) => {
        res.json(Object.values(data[0])[0]);
    });
}

function getDeletedRecords(req, res) {
    const projectId = req.params.projectId;
    const startDate = req.params.startDate;
    const endDate = req.params.endDate;
    console.log(projectId, startDate, endDate);
    knex("nilsb_statistics").count().where({
        statisticsType: 1,
        projectId: projectId
    })
        .where('statisticsDate', '>=', startDate)
        .where('statisticsDate', '<=', endDate).then((data) => {
        res.json(Object.values(data[0])[0]);
    });
}

export default {
    getAddedRecords,
    getDeletedRecords,
    getModifiedRecords
}
