/*
* Datenbankmodell für Statistiken

create table nilsb_timeRecordings
(
    timeRecordingsid       int auto_increment
        primary key,
    employeesId            int           null,
    projectsId             int           null,
    recordingTypesId       int           null,
    timeRecordingsDate     date          null,
    timeRecordingsDuration decimal(5, 2) null,
    timeRecordingsComment  text          null
);

create table nilsb_recordingTypes
(
    recordingTypesId   int auto_increment
        primary key,
    recordingTypesName varchar(50) not null
);

create table nilsb_projects
(
    projectsId   int auto_increment
        primary key,
    projectsName varchar(50) not null
);

create table nilsb_employees
(
    employeesId        int auto_increment
        primary key,
    employeesFirstname varchar(50) not null,
    employeesLastname  varchar(50) not null
);

 create table nilsb_statistics
 (
   statisticsId int auto_increment primary key,
   statisticsType int not null,
   statisticsDate date not null,
   projectId int not null
  );
 */


function addDeletion(id){
    knex("nilsb_statistics").insert({
        statisticsType: 1,
        projectId: id,
        statisticsDate: new Date().toISOString().split('T')[0]
    }).then((data) => {
        console.log('addDeletion', data);
    })
}

function addCreation(id){
    knex("nilsb_statistics").insert({
        statisticsType: 2,
        projectId: id,
        statisticsDate: new Date().toISOString().split('T')[0]
    }).then((data) => {
        console.log('addCreation', data);
    })
}

function addModification(id){
    knex("nilsb_statistics").insert({
        statisticsType: 3,
        projectId: id,
        statisticsDate: new Date().toISOString().split('T')[0]
    }).then((data) => {
        console.log('addModification', data);
    })
}

export {
    addDeletion,
    addCreation,
    addModification
}
