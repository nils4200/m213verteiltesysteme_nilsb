CREATE TABLE nilsb_projects (
                                projectsId INT AUTO_INCREMENT PRIMARY KEY,
                                projectsName VARCHAR(255) NOT NULL
);

CREATE TABLE nilsb_employees (
                                 employeesId INT AUTO_INCREMENT PRIMARY KEY,
                                 employeesFirstname VARCHAR(255) NOT NULL,
                                 employeesLastname VARCHAR(255) NOT NULL
);

CREATE TABLE nilsb_recordingTypes (
                                      recordingTypesId INT AUTO_INCREMENT PRIMARY KEY,
                                      recordingTypesName VARCHAR(255) NOT NULL
);

CREATE TABLE nilsb_timeRecordings (
                                      timeRecordingsId INT AUTO_INCREMENT PRIMARY KEY,
                                      employeesId INT,
                                      projectsId INT,
                                      recordingTypesId INT,
                                      timeRecordingsDate DATE,
                                      timeRecordingsDuration DECIMAL(5, 2),
                                      timeRecordingsComment TEXT
);

CREATE TABLE nilsb_statistics (
                                  statisticsId INT AUTO_INCREMENT PRIMARY KEY,
                                  statisticsType INT,
                                  statisticsDate DATE,
                                  projectId INT
);
