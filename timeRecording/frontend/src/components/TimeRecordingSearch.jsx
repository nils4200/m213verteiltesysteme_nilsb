import React, {useState} from "react";

import timeRecordingDataService from '../services/timeRecordingDataService'

const TimeRecordingData = ({projects, setTimeRecording}) => {

    const [projectId, setProjectId] = useState('');
    const [timeRecordings, setTimeRecordings] = useState([]);

    const searchTimeRecordings = (event) => {
        event.preventDefault();
        console.log('searchTimeRecordings');
        timeRecordingDataService.searchTimeRecordings(projectId).then(timeRecordingsFound => {
            console.log('time recordings found', timeRecordingsFound);
            setTimeRecordings(timeRecordingsFound);
            showRecordings()
        })
    }

    function showRecordings() {

        if (timeRecordings.length === 0) {
            console.log("no recordings found")
            return <p>No recordings found</p>
        } else {
            const recordingList = timeRecordings.data.map((recording) => (
                <li key={recording.timeRecordingsid}>ID: {recording.timeRecordingsid}, projektId: {recording.projectsId},
                    comment: {recording.timeRecordingsComment}, duration: {recording.timeRecordingsDuration}</li>
            ));

            return (
                <ul id={"list"}>
                    {recordingList}
                </ul>
            );
        }
    }


    const handleProjectChange = (event) => {
        let id = event.target.value;
        console.log('handleProjecthange', id);
        setProjectId(id);
    }

    const handleEditTimeRecording = (timeRecording) => {
        console.log("Edit time recording:", timeRecording);
        setTimeRecording(timeRecording);
    };

    const handleDeleteTimeRecording = (id) => {
        console.log("Delete time recording with id:", id);
        timeRecordingDataService.deleteTimeRecording(id);
    };


    return (
        <div>
            <form onSubmit={searchTimeRecordings}>
                <div>
                    <div>
                        <label>Projekt:
                            <select
                                onChange={handleProjectChange}
                                value={projectId}>
                                <option value="" disabled>Projekt auswählen</option>
                                {projects.map(project => (
                                    <option key={project.projectsId} value={project.projectsId}>{project.projectsName} </option>
                                ))}
                            </select>
                        </label>

                    </div>
                </div>
                <div style={{margin: '5px'}}>
                    <button type="submit">Suchen</button>
                </div>
            </form>
            {/* show time recordings in a table / list */}
            {showRecordings()}
        </div>
    )

}

export default TimeRecordingData;
