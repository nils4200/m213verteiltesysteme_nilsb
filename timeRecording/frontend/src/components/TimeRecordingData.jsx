import React, { useState, useEffect } from "react";

import timeRecordingDataService from '../services/timeRecordingDataService'

const TimeRecordingData = ({ employees, projects, recordingTypes, timeRecording, setTimeRecording }) => {
    // all properties of the form
    // only the id of employee, project and recordingType is used / saved
    const [employeeId, setEmployeeId] = useState('');
    const [projectId, setProjectId] = useState('');
    const [recordingTypeId, setRecordingTypeId] = useState('');
    const [date, setDate] = useState('');
    const [duration, setDuration] = useState('');
    const [comment, setComment] = useState('');

    useEffect(() => {
        // set selected values for dropdowns
        setEmployeeId(timeRecording.employeeId);
        setProjectId(timeRecording.projectId);
        setRecordingTypeId(timeRecording.recordingTypeId);
        // set ohter values
        setComment(timeRecording.comment);
        setDate(timeRecording.date);
        setDuration(timeRecording.duration);
    }, []); // should render at every button click, e.g. edit

    const addEditTimeRecording = (event) => {
        event.preventDefault();
        console.log('addEditTimeRecording');
        const timeRecordingToAdd = { projectId: projectId, employeeId: employeeId, recordingTypeId: recordingTypeId, date: date, duration: duration, comment: comment };
        if (timeRecording.timeRecordingId > 0) {
            // edit
            const timeRecordingToEdit = { ...timeRecordingToAdd, timeRecordingId: timeRecording.timeRecordingId }
            timeRecordingDataService.editTimeRecording(timeRecordingToEdit).then(id => {
                console.log('time recording edited with id', id);
                setTimeRecording(timeRecordingToEdit);
            })
        } else {
            // add
            timeRecordingDataService.addTimeRecording(timeRecordingToAdd).then(id => {
                console.log('time recording added with id', id);
                timeRecordingToAdd.timeRecordingId = id;
                setTimeRecording(timeRecordingToAdd);
            })
        }
    }

    const handleCommentChange = (event) => {
        let comment = event.target.value;
        console.log('handleCommentChange', comment);
        setComment(comment);
    };

    const handleDurationChange = (event) => {
        let duration = event.target.value;
        console.log('handleDurationChange', duration);
        setDuration(duration);
    };

    const handleDateChange = (event) => {
        let date = event.target.value;
        console.log('handleDateChange', date);
        setDate(date);
    };

    const handleEmployeeChange = (event) => {
        let id = event.target.value;
        console.log('handleEmployeeChange', id);
        setEmployeeId(id);
    }

    const handleProjectChange = (event) => {
        let id = event.target.value;
        console.log('handleProjecthange', id);
        setProjectId(id);
    }

    const handleRecordingTypeChange = (event) => {
        let id = event.target.value;
        console.log('handleRecordingTypeChange', id);
        setRecordingTypeId(id);
    }

    return (
        <form onSubmit={addEditTimeRecording}>
            <div>Projekt
                <select
                    onChange={handleProjectChange}
                    value={projectId} >
                    <option value="" disabled>Projekt auswählen</option>
                    {projects.map(project => (
                        <option key={project.projectsId} value={project.projectsId} >{project.projectsName} </option>
                    ))}
                </select>
            </div>
            <div>Art der Zeiterfassung
                <select
                    onChange={handleRecordingTypeChange}
                    value={recordingTypeId} >
                    <option value="" disabled>Zeiterfassungsart auswählen</option>
                    {recordingTypes.map(type => (
                        <option key={type.recordingTypesId} value={type.recordingTypesId}>{type.recordingTypesName}</option>
                    ))}
                </select>
            </div>
            <div>Mitarbeiter
                <select
                    onChange={handleEmployeeChange}
                    value={employeeId} >
                    <option value="" disabled>Mitarbeiter auswählen</option>
                    {employees.map(employee => (
                        <option key={employee.employeesId} value={employee.employeesId}>{employee.employeesFirstname} {employee.employeesLastname}</option>
                    ))}
                </select>
            </div>
            <div>
                <label>
                    Datum:
                    <input
                        type="date"
                        value={date}
                        onChange={handleDateChange}
                    />
                </label>
            </div>
            <div>
                <label>
                    Zeit in Stunden:
                    <input
                        type="number"
                        value={duration}
                        onChange={handleDurationChange}
                    />
                </label>
            </div>
            <div>
                <label>
                    Bemerkung:
                    <input
                        type="text"
                        value={comment}
                        onChange={handleCommentChange}
                    />
                </label>
            </div>
            <div>
                <button style={{ margin: '5px' }} type="submit">Speichern</button>
            </div>
        </form>
    )

}

export default TimeRecordingData;
