import React, { useState, useEffect } from 'react';
import { getDeletedRecords, getNewRecords, getModifiedRecords } from '../services/statisticService';

const TimeRecordingStatistics = ({projects, statisticTypes}) => {
    const [deletedRecords, setDeletedRecords] = useState(0);
    const [newRecords, setNewRecords] = useState(0);
    const [modifiedRecords, setModifiedRecords] = useState(0);
    const [projectId, setProjectId] = useState(''); // State to store selected project ID
    const [recordType, setRecordType] = useState('deleted'); // State to store selected record type
    const [statisticType, setStatisticType] = useState([]);
    const [date1, setDate1] = useState('2021-01-01');
    const [date2, setDate2] = useState('3000-01-01');
    console.log(projects)
    console.log(statisticTypes)

    const handleDate1Change = (e) => {
        setDate1(e.target.value);
    }

    const handleDate2Change = (e) => {
        setDate2(e.target.value);
    }

    const handleProjectChange = (e) => {
        setProjectId(e.target.value);
    };

    const handleStatisticTypeChange = (e) => {
        setRecordType(e.target.value);
    };

    function searchRecords(){
    switch (recordType) {
        case 'deleted':
            getDeletedRecords(projectId, date1, date2).then((response) => {
                setDeletedRecords(response);
            });
            break;
        case 'new':
            getNewRecords(projectId, date1, date2).then((response) => {
                setNewRecords(response);
            }
            );
            break;
        case 'modified':
            getModifiedRecords(projectId, date1, date2).then((response) => {
                setModifiedRecords(response);
            });
    }
    }


    return (
        <div>
            <div>
                <label htmlFor="projectSelect">Select Project:</label>
                <select id="projectSelect" onChange={handleProjectChange}>
                    <option value="">Select Project</option>
                    {projects.map(project => (
                        <option key={project.projectsId} value={project.projectsId}>
                            {project.projectsName}
                        </option>
                    ))}
                </select>
            </div>
            <div>
                <label htmlFor="statisticTypeSelect">Select Record Type:</label>
                <select id="statisticTypeSelect" onChange={handleStatisticTypeChange}>
                    <option value="">Select Statistic Type</option>
                    {statisticTypes.map((type) => (
                        <option key={type.id} value={type.name}>
                            {type.name}
                        </option>
                    ))}
                </select>
                <label>
                    Von:
                    <input
                        type="date"
                        value={date1}
                        onChange={handleDate1Change}
                    />
                </label>
                <label>
                    Bis:
                    <input
                        type="date"
                        value={date2}
                        onChange={handleDate2Change}
                    />
                </label>
            </div>
            <button onClick={() => searchRecords()}>Get Statistics</button>
            <div>
                {recordType === 'deleted' && <h2>Deleted Records: {deletedRecords}</h2>}
                {recordType === 'new' && <h2>New Records: {newRecords}</h2>}
                {recordType === 'modified' && <h2>Modified Records: {modifiedRecords}</h2>}
            </div>
        </div>
    );
};

export default TimeRecordingStatistics;
