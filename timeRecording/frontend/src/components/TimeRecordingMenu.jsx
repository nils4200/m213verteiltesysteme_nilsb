import React from 'react';


const TimeRecordingMenu = ({menuItems, setSelectedItem}) => {

    return (
        <div id="menu">
            <nav>
                {

                    menuItems.map((item) => (
                        <button
                            key={item.id}
                            onClick={() => setSelectedItem(item.id)}>
                            {item.name}
                        </button>
                    ))
                }
            </nav>
        </div>
    )
}
export default TimeRecordingMenu