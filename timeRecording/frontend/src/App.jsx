// eslint-disable-next-line no-unused-vars
import React, {useEffect, useState} from "react";
import authenticationService from './services/authenticationService';
import './App.css'
import TimeRecordingData from './components/TimeRecordingData';
import TimeRecordingSearch from './components/TimeRecordingSearch';

import miscService from './services/timeRecordingMiscService';
import TimeRecordingMenu from "./components/TimeRecordingMenu.jsx";
import TimeRecordingStatistics from "./components/TimeRecordingStatistic.jsx";

function App() {
    const [statisticTypes, setStatisticTypes] = useState([]);
    const [token, setToken] = useState(null);
    // eslint-disable-next-line no-unused-vars
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [employees, setEmployees] = useState([])
    const [projects, setProjects] = useState([])
    const [recordingTypes, setRecordingTypes] = useState([])
    const [timeRecording, setTimeRecording] = useState({
        projectId: "1",
        employeeId: "1",
        recordingTypeId: "1",
        date: new Date().toISOString().split('T')[0],
        duration: 4,
        comment: "Comment"
    });
    const [selectedItem, setSelectedItem] = useState(["menuWelcome"]);
    //wilkommen, Zeiterfassung Dateneingabe, Zeiterfassung Suche
    const menuItems = [
        {name: "Wilkommen", id: 'menuWelcome'},
        {name: "Zeiterfassung Dateneingabe", id: 'menuRecording'},
        {name: "Zeiterfassung Suche", id: 'menuSearch'},
        {name: "Statistiken", id: 'statistics'},
    ];

    useEffect(() => {
        authenticationService.initKeycloak()
            .then((token) => {
                console.log("token: ", token);
                setToken(token);
                setIsModalOpen(false);
            })
            .catch((error) => {
                console.error("Error initializing Keycloak:", error);
                setIsModalOpen(true);
            });
    }, []);


    useEffect(() => {
        if (token !== null && token.length > 0) {
            readProjects();
            readEmployees();
            readRecordingTypes();
            readStatisticTypes();
       } else {
            console.log("token is null or empty");
            setIsModalOpen(true);
        }
    }, [token]);


    const readEmployees = () => {
        miscService.readAllEmployees()
            .then(data => {
                console.log('readEmployees in App.jsx: ', data);
                setEmployees(data);
            })
            .catch(error => console.error("Error reading employees:", error));
    };

    const readRecordingTypes = () => {
        miscService.readAllRecordingTypes()
            .then(data => {
                console.log('readRecordingTypes in App.jsx: ', data);
                setRecordingTypes(data);
            })
            .catch(error => console.error("Error reading recording types:", error));
    };

    const readProjects = () => {
        miscService.readAllProjects()
            .then(data => {
                console.log('readProjects in App.jsx: ', data);
                setProjects(data);
            })
            .catch(error => console.error("Error reading projects:", error));
    };
    // eslint-disable-next-line no-unused-vars
    const readStatisticTypes = () => {
        miscService.readAllStatisticTypes()
            .then(data => {
                console.log('read statisticTypes in App.jsx: ', data);
                setStatisticTypes(data);
            })
            .catch(error => console.error("Error reading recording types:", error));
    };

    return (
        <div>

            <TimeRecordingMenu menuItems={menuItems} setSelectedItem={setSelectedItem}/>
            {selectedItem === 'menuWelcome' && <h1>Wilkommen</h1>}
            {selectedItem === 'menuRecording' &&
                <TimeRecordingData employees={employees} projects={projects} recordingTypes={recordingTypes}
                                   timeRecording={timeRecording} setTimeRecording={setTimeRecording}/>}
            {selectedItem === 'menuSearch' &&
                <TimeRecordingSearch projects={projects} setTimeRecording={setTimeRecording}/>}
            {selectedItem === 'statistics' && <TimeRecordingStatistics projects={projects} statisticTypes={statisticTypes}/>}


        </div>
    )
}

export default App
