import axios from 'axios';
import authenticationService from "./authenticationService.js";

const token = authenticationService.getToken();

const API_URL = 'http://localhost:3000/api/';
const baseUrlData = API_URL + 'data/';

const addTimeRecording = timeRecording => {
    const url = baseUrlData + 'add';
    return axios.post(url, timeRecording);
}

const editTimeRecording = timeRecording => {
    const url = baseUrlData + 'edit';
    return axios.put(url, timeRecording);
}

const deleteTimeRecording = timeRecordingId => {
    const url = baseUrlData + 'delete/' + timeRecordingId;
    return axios.delete(url);
}

const searchTimeRecordings = projectId => {
    const url = baseUrlData + 'search/' + projectId;
    const data = axios.get(url, {
        headers: {
            authorization: `Bearer ${token}`
        }
    })
    return data
}

export default {
    addTimeRecording,
    editTimeRecording,
    searchTimeRecordings,
    deleteTimeRecording
};
