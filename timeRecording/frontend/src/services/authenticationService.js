import Keycloak from 'keycloak-js';

const keycloak = new Keycloak({
    url: 'http://localhost:8080',
    realm: 'ims3i',
    clientId: 'timeRecordingFrontend'
});

let isKeycloakInitialized = false;

const initKeycloak = () => {
    let promise = new Promise(function (resolve, reject) {
        try {
            if (isKeycloakInitialized) {
                console.log('KeyCloak is already initialized');
            }
            console.log('initKeycloak');
            isKeycloakInitialized = true;
            keycloak.init({ onLoad: 'login-required' }).then(authenticated => {
                console.log(`User is ${authenticated ? 'authenticated' : 'not authenticated'}`);
                console.log('token received:', keycloak.token);
                resolve(keycloak.token);
            }).catch(error => {
                console.error("Error 1 initializing Keycloak:", error)
                reject(-1);
            });
        } catch (error) {
            console.error("Error 2 initializing Keycloak:", error)
            reject(-1);
        }
    });
    return promise;
};

const minimumValidityTimeInSeconds = 300;

const getToken = () => {
    keycloak.updateToken(minimumValidityTimeInSeconds).then(valid => {
        if (valid){
            console.log("token still valid")
        } else {
            keycloak.updateToken().then(refreshed => {
                if (refreshed) {
                    console.log("token refreshed: ", refreshed)
                } else {
                    console.log("token not refreshed: ", refreshed)
                }
            }).catch(error => {
                console.error("Error refreshing token:", error)
            })
        }
    })
    return keycloak.token;
}

export default {
    initKeycloak,
    getToken
}