import axios from 'axios';
const API_URL = 'http://localhost:3000';

export const getDeletedRecords = async (projectId, startDate, endDate) => {
    try {
        const response = await axios.get(API_URL+`/api/statistics/deleted/${projectId}/${startDate}/${endDate}`);
        return response.data;
    } catch (error) {
        throw new Error(error.response.data.error || 'Failed to fetch deleted records');
    }
};

export const getModifiedRecords = async (projectId, startDate, endDate) => {
    try {
        const response = await axios.get(API_URL+`/api/statistics/modified/${projectId}/${startDate}/${endDate}`);
        return response.data;
    } catch (error) {
        throw new Error(error.response.data.error || 'Failed to fetch modified records');
    }
};

export const getNewRecords = async (projectId, startDate, endDate) => {
    try {
        const response = await axios.get(API_URL+`/api/statistics/added/${projectId}/${startDate}/${endDate}`);
        return response.data;
    } catch (error) {
        throw new Error(error.response.data.error || 'Failed to fetch new records');
    }
}
