import axios from 'axios';
import authenticationService from "./authenticationService.js";

const baseUrlMisc = 'http://localhost:3000/api/misc/';

const readAllEmployees = () => {
    let token = authenticationService.getToken();
    const url = baseUrlMisc + 'employee/';
    const data = axios.get(url, {
        headers: {
            authorization: `Bearer ${token}`
        }
    })
    return data.then(response => {
        return response.data
    })
}

const readAllProjects = () => {
    let token = authenticationService.getToken();
    console.log("readallprojects")
    const url = baseUrlMisc + 'project/';
    const data = axios.get(url, {
        headers: {
            authorization: `Bearer ${token}`
        }
    })
    return data.then(response => {
        return response.data
    })

}

const readAllRecordingTypes = () => {
    let token = authenticationService.getToken();
    console.log("readallprojects")
    const url = baseUrlMisc + 'type/';
    const data = axios.get(url, {
        headers: {
            authorization: `Bearer ${token}`
        }
    })
    return data.then(response => {
        return response.data
    })

}
const readAllStatisticTypes = () => {
    console.log("getting statistic types")
    let token = authenticationService.getToken();
    const url = baseUrlMisc + 'statisticType/';
    const data = axios.get(url, {
        headers: {
            authorization: `Bearer ${token}`
        }
    })
    return data.then(response => {
        console.log("statistictypesService: ", response.data)
        return response.data
    })
}


export default {
    readAllEmployees,
    readAllProjects,
    readAllRecordingTypes,
    readAllStatisticTypes
};
